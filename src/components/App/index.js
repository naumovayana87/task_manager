import React, {useState} from 'react';

const App=()=>{
  let initTask=[
    {id:1, title:'open you eyes', done:false},
    {id:2, title:'check smartphone', done:true},
    {id:3, title:'brash you teeth', done:true},
  ]
  const [tasks,setTask]=useState(initTask)
  const [newTasks,setNewTasks]=useState('')
const enterNewTask=(e)=>{
    setNewTasks(e.target.value)
}
const addNewTask=()=>{
    const taskObj={id:tasks.length+1, title:newTasks, done:false}
    setTask([...tasks,taskObj])
  setNewTasks('')
}

  const changeTaskStatus=(element)=>{
    setTask(tasks.map(item=>item.id===element.id?{...item,done:!item.done}:item))
  }
  return (
    <>
      <div>
        <input type='text' value={newTasks} onChange={enterNewTask}/>
        <button onClick={addNewTask}>Add new Task</button>
      </div>
        <ul className='task-list'>
          {
          tasks.map(el=><li key={el.id} className={el.done?'task-done':''} onClick={()=>changeTaskStatus(el)}>{el.title}</li>)
          }
        </ul>
    </>
  );
}

export default App;
